import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal } from 'semantic-ui-react';
import { toggleEditingPost } from 'src/containers/Thread/actions';
import Spinner from 'src/components/Spinner';
import { Form, Button } from 'semantic-ui-react';

class EditPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.post.id,
            body: props.post.body,
            open: true
        };
    }

    componentWillUnmount () {
        this.setState({ body: '' });
    }

    closeModal = () => {
        this.props.toggleEditingPost();
    };

    handleEditPost = async () => {
        const { id, body } = this.state;
        if (!body) {
            return;
        }

        await this.props.editPost( id, body );

    };
    render() {
        const { post } = this.props;
        return (
            <Modal dimmer="blurring" centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Modal.Content>

                            <Form reply onSubmit={this.handleEditPost}>
                                <Form.TextArea
                                    defaultValue={post.body}
                                    onChange={ev => this.setState({ body: ev.target.value })}
                                />
                                <Button type="submit" className="positive" content="Edit" labelPosition="left" icon="edit" primary />
                            </Form>
                        </Modal.Content>
                    )
                    : <Spinner />
                }
            </Modal>
        );
    }
}

EditPost.propTypes = {
    editPost: PropTypes.func.isRequired,
    post: PropTypes.object.isRequired,
    toggleEditingPost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
    post: rootState.posts.editingPost,
    userId: rootState.profile.user.id
});
const actions = { toggleEditingPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditPost);

