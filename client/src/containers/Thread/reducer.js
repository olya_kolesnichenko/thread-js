import {
    SET_ALL_POSTS,
    LOAD_MORE_POSTS,
    ADD_POST,
    REMOVE_POST,
    SET_EXPANDED_POST,
    SET_EDITING_POST,
    START_EDIT_COMMENT,
    STOP_EDIT_COMMENT
} from './actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALL_POSTS:
            return {
                ...state,
                posts: action.posts,
                hasMorePosts: Boolean(action.posts.length)
            };
        case LOAD_MORE_POSTS:
            return {
                ...state,
                posts: [...(state.posts || []), ...action.posts],
                hasMorePosts: Boolean(action.posts.length)
            };
        case ADD_POST:
            return {
                ...state,
                posts: [action.post, ...state.posts]
            };
        case REMOVE_POST:
            return {
                ...state,
                posts: state.posts.filter((item) => item.id !== action.payload)
            };
        case SET_EXPANDED_POST:
            return {
                ...state,
                expandedPost: action.post
            };
        case SET_EDITING_POST:
            return {
                ...state,
                editingPost: action.payload
            };
        case START_EDIT_COMMENT:
            return {
                ...state,
                editingComment: action.payload
            };
        case STOP_EDIT_COMMENT:
            return {
                ...state,
                editingComment: {}
            };
        default:
            return state;
    }
};
