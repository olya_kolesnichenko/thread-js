import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
    ADD_POST,
    REMOVE_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST,
    SET_EDITING_POST,
    START_EDIT_COMMENT,
    STOP_EDIT_COMMENT,
    EDIT_COMMENT
} from './actionTypes';

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const removePostAction = postId => ({
    type: REMOVE_POST,
    payload: postId
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

const setEditingPostAction = post => ({
    type: SET_EDITING_POST,
    payload: post
});

const setEditingComment = comment => ({
    type: START_EDIT_COMMENT,
    payload: comment
});
const clearEditComment = () => ({
    type: STOP_EDIT_COMMENT
});

// const editComment = (comment) => ({
//     type: EDIT_COMMENT,
//     payload: comment
// });

export const startEditComment = (comment) => async (dispatch) => {
    dispatch(setEditingComment(comment));
};

export const stopEditComment = () => async (dispatch) => {
    dispatch(clearEditComment());
};


export const loadPosts = filter => async (dispatch) => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const { posts: { posts } } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts
        .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const toggleEditingPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setEditingPostAction(post));
};

export const removePost = postId => async (dispatch) => {
    const post = await postService.removePost(postId);
    dispatch(removePostAction(postId));
};

export const editPost = (postId, data) => async (dispatch, getRootState) => {
    const currentPost = await postService.editPost({postId, data});

    const mapUpdate = post => ({
        ...post,
        body: currentPost.body
    });
    const { posts: { posts, editingPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapUpdate(post)));
    dispatch(setPostsAction(updated));

    if (editingPost && editingPost.id === postId) {
        dispatch(setEditingPostAction(null));
    }
};

export const likePost = postId => async (dispatch, getRootState) => {
    const { likesCount, dislikesCount } = await postService.likePost(postId);

    const mapLikes = post => ({
        ...post,
        likeCount: likesCount,
        dislikeCount: dislikesCount
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

 export const dislikePost = postId => async (dispatch, getRootState) => {
     const { likesCount, dislikesCount } = await postService.dislikePost(postId);

     const mapDislikes = post => ({
         ...post,
         dislikeCount: dislikesCount,
         likeCount: likesCount,
     });

     const { posts: { posts, expandedPost } } = getRootState();
     const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

     dispatch(setPostsAction(updated));

     if (expandedPost && expandedPost.id === postId) {
         dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
     }
 };

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId
        ? post
        : mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const removeComment = id => async (dispatch, getRootState) => {
    const comment = await commentService.getComment(id);
    const commentDeleted = await commentService.editComment(id, {...comment, isDeleted: true});

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) - 1,
        comments: post.comments ?  post.comments.filter((item) => item.id !== id) :[]
    });

    const { posts: { posts, expandedPost } } = getRootState();
    const updated = posts.map(post => (post.id === comment.postId && !post.comments
        ? mapComments(post)
        : post));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));

    }

};

export const likeComment = commentId => async (dispatch, getRootState) => {
    const { likesCount, dislikesCount } = await commentService.likeComment(commentId);

    const mapLikes = comment => ({
        ...comment,
        likeCount: likesCount,
        dislikeCount: dislikesCount
    });

    const { posts: { expandedPost } } = getRootState();
    const updated = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)));


    dispatch(setExpandedPostAction({...expandedPost, comments: updated}));

};

export const dislikeComment = commentId => async (dispatch, getRootState) => {
    const { likesCount, dislikesCount } = await commentService.dislikeComment(commentId);

    const mapDislikes = comment => ({
        ...comment,
        likeCount: likesCount,
        dislikeCount: dislikesCount
    });

    const { posts: { expandedPost } } = getRootState();
    const updated = expandedPost.comments.map(comment => (comment.id !== commentId ? comment : mapDislikes(comment)));


    dispatch(setExpandedPostAction({...expandedPost, comments: updated}));
};
export const updateComment = currentComment => async (dispatch, getRootState) => {
    const newCommentText = await commentService.editComment(currentComment.id, currentComment);

    const mapComments = comment => ({
        ...comment,
        body: currentComment.body
        });


    const { posts: { expandedPost } } = getRootState();
    const updated = expandedPost.comments.map(comment => (comment.id !== currentComment.id ? comment : mapComments(comment)));


    dispatch(setExpandedPostAction({...expandedPost, comments: updated}));
    dispatch(clearEditComment());

};
