import React, { PureComponent, createRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Comment as CommentUI, Card, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import {  updateComment, startEditComment, stopEditComment } from 'src/containers/Thread/actions';

import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

class Comment extends PureComponent {
    textInput = createRef();

    _editComment = () => {
        const { stopEditComment, startEditComment, comment, editingComment } = this.props;
        if (editingComment && editingComment.id === comment.id) {
            stopEditComment();
        } else {
            startEditComment(comment);
        }
    };

    _editingComment = (event) => {
        const { startEditComment, stopEditComment, comment, updateComment } = this.props;

        if (event.key === "Enter" && event.target.value !== "") {
            updateComment({ ...comment, body: event.target.value });
        } else if (event.key === "Escape") {
            stopEditComment();
        } else {
            startEditComment({
                ...comment,
                body: event.target.value
            });
        }
    };

    _changeCommentText = (event) => {
        const { startEditComment, comment } = this.props;

        startEditComment({
            ...comment,
            body: event.target.value
        });
    };

    _removeComment = () => {
        const { removeComment, id } = this.props;

        removeComment(id);
    };
    _likeComment = () => {
        const { likeComment, id } = this.props;

        likeComment(id);
    };
    _dislikeComment = () => {
        const { dislikeComment, id } = this.props;

        dislikeComment(id);
    };
    componentDidUpdate = () => {
        this.textInput.current.focus();
    };
    render() {
        const {comment: {body, createdAt, user, id, likeCount, dislikeCount }, userId, editingComment} = this.props;
        const isMyComment = (userId === user.id);
        const isEdit = editingComment ? editingComment.id === id : false;
        const text = isEdit ? editingComment.body : body;
        const date = moment(createdAt).fromNow();

        const cross = isMyComment ? (
            <span className={styles.cross} onClick={ this._removeComment }>X</span>
        ) : null;
        const edit = isMyComment ? (
            <Label basic size="big" as="a" className={styles.toolbarBtn} onClick={ this._editComment }>
                <Icon name="edit outline"/>
            </Label>
        ) : null;

        return (
            <CommentUI className={styles.comment}>
                {cross}
                <CommentUI.Avatar src={getUserImgLink(user.image)}/>
                <CommentUI.Content>
                    <CommentUI.Author as="a">
                        {user.username}
                    </CommentUI.Author>
                    <CommentUI.Metadata>
                        {date}
                    </CommentUI.Metadata>
                    <input
                        disabled={!isEdit}
                        type="text"
                        ref={this.textInput}
                        value={text}
                        onChange={this._changeCommentText}
                        onKeyUp={this._editingComment}
                    />
                    <Card.Content extra>
                        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={ this._likeComment }>
                            <Icon name="thumbs up"/>
                            { likeCount }
                        </Label>
                        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={ this._dislikeComment }>
                            <Icon name="thumbs down"/>
                            { dislikeCount }
                        </Label>
                        {edit}
                    </Card.Content>
                </CommentUI.Content>
            </CommentUI>
        );
    }
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired,
    removeComment: PropTypes.func.isRequired,
    likeComment: PropTypes.func.isRequired,
    dislikeComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
    userId: rootState.profile.user.id,
    editingComment: rootState.posts.editingComment
});
const actions = { startEditComment, stopEditComment, updateComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Comment);
// Comment.propTypes = {
//     comment: PropTypes.objectOf(PropTypes.any).isRequired,
//     removeComment: PropTypes.func.isRequired,
//     editComment: PropTypes.func.isRequired
// };

