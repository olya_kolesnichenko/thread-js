import {
    createStore,
    applyMiddleware,
    compose,
    combineReducers
} from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import { createBrowserHistory } from 'history';
// import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import threadReducer from './containers/Thread/reducer';
import profileReducer from './containers/Profile/reducer';

export const history = createBrowserHistory();

const logger = createLogger({
    duration:  true,
    collapsed: true,
    colors:    {
        title:     () => '#139BFE',
        prevstate: () => '#1C5FAF',
        action:    () => '#149945',
        nextState: () => '#A47104',
        error:     () => '#FF0005',
    },

});

const initialState = {};

const middlewares = [
    thunk,
    routerMiddleware(history),
    logger
];

const composedEnhancers = compose(
    applyMiddleware(...middlewares)
);

const reducers = {
    posts: threadReducer,
    profile: profileReducer
};

const rootReducer = combineReducers({
    router: connectRouter(history),
    ...reducers
});

const store = createStore(
    rootReducer,
    initialState,
    composedEnhancers
);

export default store;
