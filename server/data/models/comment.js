export default (orm, DataTypes) => {
    const Comment = orm.define('comment', {
        body: {
            allowNull: false,
            type: DataTypes.TEXT
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
        isDeleted: {
            allowNull: false,
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
    }, {});

    return Comment;
};
