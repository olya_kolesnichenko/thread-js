import sequelize from '../db/connection';
import {PostModel, CommentModel, UserModel, ImageModel, PostReactionModel, CommentReactionModel} from '../models/index';
import BaseRepository from './base.repository';


const likeCase = bool => `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
    async getPosts(filter) {
        const {
            from: offset,
            count: limit,
            userId
        } = filter;

        const where = {isDeleted: false};
        if (userId) {
            Object.assign(where, {userId});
        }


        return this.model.findAll({
            where,
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."isDeleted" = false)`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
                ]
            },
            include: [{
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: PostReactionModel,
                attributes: [],
                duplicating: false
            }],
            group: [
                'post.id',
                'image.id',
                'user.id',
                'user->image.id'
            ],
            order: [['createdAt', 'DESC']],
            offset,
            limit
        });
    }

    getPostById(id) {
        const post = this.model.findOne({
            group: [
                'post.id',
                'comments.id',
                'comments->user.id',
                'comments->user->image.id',
                'user.id',
                'user->image.id',
                'image.id'
            ],
            where: {id},
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."isDeleted" = false)`), 'commentCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(true))), 'likeCount'],
                    [sequelize.fn('SUM', sequelize.literal(likeCase(false))), 'dislikeCount']
                ]
            },
            include: [{
                model: CommentModel,

                where: {isDeleted: false},
                attributes: {
                    include: [
                          [sequelize.literal(`
                         (SELECT COUNT(*)
                         FROM "commentReactions"
                         WHERE "commentReactions"."commentId" = "comments"."id" AND "commentReactions"."isLike" = true)`), 'likeCount'],
                        [sequelize.literal(`
                         (SELECT COUNT(*)
                         FROM "commentReactions"
                         WHERE "commentReactions"."commentId" = "comments"."id" AND "commentReactions"."isLike" = false)`), 'dislikeCount'],
                    ]
                },
                required: false,
                include: [{
                    model: UserModel,
                    attributes: ['id', 'username'],
                    include: {
                        model: ImageModel,
                        attributes: ['id', 'link']
                    }
                }, {
                    model: CommentReactionModel,
                    attributes: []
                }]
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: PostReactionModel,
                attributes: []
            }]
        });

        return post;
    }
}

export default new PostRepository(PostModel);
