import bcrypt from 'bcrypt';

const saltRounds = 10;

export default {
    encrypt: data => data, //bcrypt.hash(data, saltRounds),
    encryptSync: data => data, //bcrypt.hashSync(data, saltRounds),
    compare: (data, encrypted) => data === encrypted//bcrypt.compare(data, encrypted)
};
