import jwt from 'jsonwebtoken';
import { secret, expiresIn } from '../config/jwt.config';

export default {
    createToken: data => jwt.sign(data, secret, { expiresIn }),
    get: data => jwt.verify(data.split(' ')[1], secret).id
};
