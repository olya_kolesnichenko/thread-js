import postRepository from '../../data/repositories/post.repository';
import postReactionRepository from '../../data/repositories/post-reaction.repository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const updatePost = (postId, data) => postRepository.updateById(postId, data);

export const create = (userId, post) => postRepository.create({
    ...post,
    userId
});

export const setReaction = async (userId, { postId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
        ? postReactionRepository.deleteById(react.id)
        : postReactionRepository.updateById(react.id, { isLike }));

    const reaction = await postReactionRepository.getPostReaction(userId, postId);

    const result = reaction
        ? await updateOrDelete(reaction)
        : await postReactionRepository.create({ userId, postId, isLike });

    // the result is an integer when an entity is deleted
    const likeDislike = {
        liked: await postReactionRepository.getPostReactionLike(userId, postId, true) !== null,
        disliked: await postReactionRepository.getPostReactionLike(userId, postId, false) !== null,
        likesCount: (await postReactionRepository.getPostReactionLikeCount( postId, true)).length,
        dislikesCount: (await postReactionRepository.getPostReactionLikeCount( postId, false)).length
    };
    return likeDislike;//Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};
