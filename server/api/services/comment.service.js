import commentRepository from '../../data/repositories/comment.repository';
import commentReactionRepository from '../../data/repositories/comment-reaction.repository';

export const create = (userId, comment) => commentRepository.create({
    ...comment,
    userId
});
export const updateComment= (comment) => commentRepository.updateById(comment.id, comment);
export const editComment= (commentId, data) => commentRepository.updateById(commentId, data);

export const getCommentById = id => commentRepository.getCommentById(id);

export const setReaction = async (userId, { commentId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react => (react.isLike === isLike
        ? commentReactionRepository.deleteById(react.id)
        : commentReactionRepository.updateById(react.id, { isLike }));

    const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

    const result = reaction
        ? await updateOrDelete(reaction)
        : await commentReactionRepository.create({ userId, commentId, isLike });

    // the result is an integer when an entity is deleted
    const likeDislike = {
        liked: await commentReactionRepository.getCommentReactionLike(userId, commentId, true) !== null,
        disliked: await commentReactionRepository.getCommentReactionLike(userId, commentId, false) !== null,
        likesCount: (await commentReactionRepository.getCommentReactionLikeCount( commentId, true)).length,
        dislikesCount: (await commentReactionRepository.getCommentReactionLikeCount( commentId, false)).length
    };
    return likeDislike;//Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};
